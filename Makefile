PROJECT_NAME:= $(shell poetry version | cut -d' ' -f1)
VERSION:= $(shell poetry version | grep -o '[0-9.]*')

.PHONY: lint
lint:
	poetry run flake8

.PHONY: test
test:
	poetry run pytest

.PHONY: publish
publish: check_published test clean
	poetry publish --build

.PHONY: publish_test
publish_test: check_published_test test clean
	poetry config repositories.test-pypi "https://test.pypi.org/legacy/"
	poetry publish --build --repository=test-pypi

.PHONY: check_published
check_published:
	@if curl --output /dev/null --silent --head --fail "https://pypi.org/project/$(PROJECT_NAME)/$(VERSION)/"; then \
		echo "$(PROJECT_NAME) v$(VERSION) is already published"; \
		exit 1; \
	fi

.PHONY: check_published_test
check_published_test:
	@if curl --output /dev/null --silent --head --fail "https://test.pypi.org/project/$(PROJECT_NAME)/$(VERSION)/"; then \
		echo "$(PROJECT_NAME) v$(VERSION) is already published"; \
		exit 1; \
	fi

.PHONY: clean
clean:
	find . -name "*.pyc" -delete
	find . -name "__pycache__" -delete
