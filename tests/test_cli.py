import re
from unittest.mock import PropertyMock, patch

from aws_credentials.cli import main
from aws_credentials.manager import Manager


def test_list(capsys, iam, stub_list):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)

    key = list_response['AccessKeyMetadata'][0]['AccessKeyId']
    status = list_response['AccessKeyMetadata'][0]['Status']
    created = list_response['AccessKeyMetadata'][0]['CreateDate']

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        opts = ['list', '--aws-access-key-id', key, '--aws-secret-access-key', 'SECRET']
        main(opts)
        result = capsys.readouterr()

        assert '' == result.err
        assert key in result.out
        assert status in result.out
        assert created in result.out


def test_create(capsys, iam, stub_list, stub_create):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)
    tst_key = list_response['AccessKeyMetadata'][0]['AccessKeyId']
    create_response = stub_create(iam_stubber)

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        opts = ['create', '--aws-access-key-id', tst_key, '--aws-secret-access-key', 'SECRET']
        main(opts)
        result = capsys.readouterr()

        assert '' == result.err
        assert create_response['AccessKey']['AccessKeyId'] in result.out
        assert create_response['AccessKey']['SecretAccessKey'] in result.out


def test_activate(capsys, iam, stub_list, stub_activate):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)
    tst_key = list_response['AccessKeyMetadata'][0]['AccessKeyId']
    stub_activate(iam_stubber)

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        opts = ['activate', '--aws-access-key-id', tst_key, '--aws-secret-access-key', 'SECRET', tst_key]
        main(opts)
        result = capsys.readouterr()

        assert '' == result.err
        assert f"Activated {tst_key}\n" == result.out


def test_deactivate(capsys, iam, stub_list, stub_deactivate):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)
    tst_key = list_response['AccessKeyMetadata'][0]['AccessKeyId']
    stub_deactivate(iam_stubber)

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        opts = ['deactivate', '--aws-access-key-id', tst_key, '--aws-secret-access-key', 'SECRET', tst_key]
        main(opts)
        result = capsys.readouterr()

        assert '' == result.err
        assert f"Deactivated {tst_key}\n" == result.out


def test_delete(capsys, iam, stub_list, stub_delete):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)
    tst_key = list_response['AccessKeyMetadata'][0]['AccessKeyId']
    stub_delete(iam_stubber)

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        opts = ['delete', '--aws-access-key-id', tst_key, '--aws-secret-access-key', 'SECRET', tst_key]
        main(opts)
        result = capsys.readouterr()

        assert '' == result.err
        assert f"Deleted {tst_key}\n" == result.out


def test_rotate(capsys, iam, stub_list, stub_create, stub_deactivate):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)
    tst_key = list_response['AccessKeyMetadata'][0]['AccessKeyId']
    create_response = stub_create(iam_stubber)
    stub_deactivate(iam_stubber)

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        opts = ['rotate', '--aws-access-key-id', tst_key, '--aws-secret-access-key', 'SECRET']
        main(opts)
        result = capsys.readouterr()

        assert '' == result.err

        # Make sure no keys were deleted
        assert re.search(r'Deleted Key[\n-]*N/A', result.out) is not None

        # Make sure no keys were deactivated
        assert re.search(rf'Deactivated Key[\n-]*{tst_key}', result.out) is not None

        # Make sure the new key is displayed
        assert create_response['AccessKey']['AccessKeyId'] in result.out
        assert create_response['AccessKey']['SecretAccessKey'] in result.out
