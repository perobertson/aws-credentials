from datetime import datetime
from uuid import uuid1

import boto3
import pytest
from botocore.stub import Stubber


def _key(user=None, key_id=None, status=None, created=None):
    return {
        'UserName': user or 'user',
        'AccessKeyId': key_id or uuid1().hex[:16],
        'Status': status or 'Active',
        'CreateDate': created or datetime.now().isoformat(),
    }


def _secret_key(user=None, key_id=None, status=None, secret=None, created=None):
    return {
        'UserName': user or 'user',
        'AccessKeyId': key_id or uuid1().hex[:16],
        'Status': status or 'Active',
        'SecretAccessKey': secret or uuid1().hex,
        'CreateDate': created or datetime.now().isoformat(),
    }


@pytest.fixture()
def iam():
    client = boto3.client('iam')
    with Stubber(client) as stubber:
        yield stubber, client
        stubber.assert_no_pending_responses()


@pytest.fixture
def key():
    return _key()


@pytest.fixture
def secret_key():
    return _secret_key()


@pytest.fixture
def stub_list():
    def _stub_list(stubber, keys=None, num_keys=1):
        if keys is None:
            keys = [
                _key()
                for idx in range(0, num_keys)
            ]
        response = {
            'AccessKeyMetadata': keys,
            'ResponseMetadata': {
                'HTTPStatusCode': 200
            },
        }
        stubber.add_response('list_access_keys', response)
        return response

    return _stub_list


@pytest.fixture
def stub_create():
    def _stub_create(stubber):
        response = {
            'AccessKey': {
                'UserName': 'user',
                'AccessKeyId': uuid1().hex[:16],
                'Status': 'Active',
                'SecretAccessKey': uuid1().hex,
                'CreateDate': datetime.now().isoformat(),
            },
            'ResponseMetadata': {
                'HTTPStatusCode': 200
            },
        }
        stubber.add_response('create_access_key', response)
        return response

    return _stub_create


@pytest.fixture
def stub_activate():
    def _stub_activate(stubber):
        response = {
            'ResponseMetadata': {
                'HTTPStatusCode': 200
            },
        }
        stubber.add_response('update_access_key', response)
        return response

    return _stub_activate


@pytest.fixture
def stub_deactivate():
    def _stub_deactivate(stubber):
        response = {
            'ResponseMetadata': {
                'HTTPStatusCode': 200
            },
        }
        stubber.add_response('update_access_key', response)
        return response

    return _stub_deactivate


@pytest.fixture
def stub_delete():
    def _stub_delete(stubber):
        response = {
            'ResponseMetadata': {
                'HTTPStatusCode': 200
            },
        }
        stubber.add_response('delete_access_key', response)
        return response

    return _stub_delete
