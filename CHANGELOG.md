# Change Log

## [Unreleased]

## [1.0.0] - 2020-05-24

### Breaking Change

- Dropped support for Python 3.5

## [0.3.0] - 2020-05-19

### Changed

- Dropped usage of `click` in favour of built in `argparse`

### CLI Breaking Change

- `--access-key` has been renamed to `--aws-access-key-id` [#26]
- `--secret-key` has been renamed to `--aws-secret-access-key` [#26]
- `--session-token` has been renamed to `--aws-session-token` [#26]

## [0.2.1] - 2019-12-06

### Fixed

- Fixed rotate when using a shared credentials file - [#22]

## [0.2.0] - 2019-12-01

### CLI Breaking Change

- `access_key` and `secret_key` global options have been moved to the commands

## [0.1.3] - 2019-11-20

### Added

- Support for python >=3.5, <4.0 - [#13]

## [0.1.2] - 2019-11-13

- Minor fixes for publishing flow

## [0.1.1] - 2019-11-13

- Unreleased
- Testing publishing flow to test pypi

### Added

- Added command `rotate` - [#1]

## [0.1.0] - 2019-10-08

- Initial release
- Available commands are `activate`, `create`, `deactivate`, `delete`, `list`

[Unreleased]: https://gitlab.com/perobertson/aws-credentials/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/perobertson/aws-credentials/tags/v1.0.0
[0.3.0]: https://gitlab.com/perobertson/aws-credentials/tags/v0.3.0
[0.2.1]: https://gitlab.com/perobertson/aws-credentials/tags/v0.2.1
[0.2.0]: https://gitlab.com/perobertson/aws-credentials/tags/v0.2.0
[0.1.3]: https://gitlab.com/perobertson/aws-credentials/tags/v0.1.3
[0.1.2]: https://gitlab.com/perobertson/aws-credentials/tags/v0.1.2
[0.1.1]: https://gitlab.com/perobertson/aws-credentials/tags/v0.1.1
[0.1.0]: https://gitlab.com/perobertson/aws-credentials/tags/v0.1.0
[#1]: https://gitlab.com/perobertson/aws-credentials/merge_requests/1
[#13]: https://gitlab.com/perobertson/aws-credentials/merge_requests/13
[#22]: https://gitlab.com/perobertson/aws-credentials/merge_requests/22
[#26]: https://gitlab.com/perobertson/aws-credentials/-/merge_requests/26
